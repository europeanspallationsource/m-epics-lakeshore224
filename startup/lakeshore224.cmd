#@field PORTNAME
#@type STRING
#The port name. Should be unique within an IOC.
#
#@field IPADDR
#@type STRING
#IP or hostname of the TCP endpoint.
#
#@field IPPORT
#@type INTEGER
#Port number for the TCP endpoint.
#
#@field PREFIX
#@type STRING
#Prefix for EPICS PVs.


epicsEnvSet(PORTNAME, "PortA")
#epicsEnvSet(IPADDR, "10.0.17.7")
#epicsEnvSet(IPADDR, "127.0.0.1")

#H-Lab
#epicsEnvSet(IPADDR, "10.4.0.135")
epicsEnvSet(IPADDR, "10.4.3.125")
epicsEnvSet(IPPORT, "7777")
epicsEnvSet(PREFIX, "test")

require pvaSrv 0.12.0

require lakeshore224 local
require streamdevice 2.7.7
require recsync 1.2.0



#Specifying the TCP endpoint and port name
drvAsynIPPortConfigure("$(PORTNAME)", "$(IPADDR):$(IPPORT)")
 
#Load your database defining the EPICS records
dbLoadTemplate(lakeshore224.substitutions, "P=$(PREFIX), PORT=$(PORTNAME), ADDR=$(IPPORT)")

#Load your database defining the EPICS records for seting a curve to a channel
dbLoadTemplate(lakeshore224_channelnames.substitutions, "P=$(PREFIX), PORT=$(PORTNAME), ADDR=$(IPPORT)")



#Configure file access
drvAsynFileConfigure("calib-files", "$(REQUIRE_lakeshore224_PATH)/misc", 15000)
 
#You have to use REQUIRE_lakeshore224_PATH (provided that your module is named lakeshore224).
#The following line in the startup script loads the database that takes care of the actual curve management:
dbLoadRecords(lakeshore224_curve_management.db, "P=$(PREFIX), ASYNPORT=$(PORTNAME), ASYNCALIBPORT=calib-files")
 

##SNL callback
##Not yet put this in the substitution file...Can it be put in substitutions?
##Maybe good to have it here with teh SNL stuff..
## P for name and CHANNEL, setCHANNEL, sigCHANNEL for different numbering ...

seq callback, "name=1, P=$(PREFIX), sigCHANNEL=0, SIGNAL=KRDG, SETPOINT=SETP_S, setCHANNEL=1, CHANNEL=1"
seq callback, "name=2, P=$(PREFIX), sigCHANNEL=1, SIGNAL=KRDG, SETPOINT=SETP_S, setCHANNEL=2, CHANNEL=2"
seq callback, "name=3, P=$(PREFIX), sigCHANNEL=2, SIGNAL=KRDG, SETPOINT=SETP_S, setCHANNEL=3, CHANNEL=3"
seq callback, "name=4, P=$(PREFIX), sigCHANNEL=3, SIGNAL=KRDG, SETPOINT=SETP_S, setCHANNEL=4, CHANNEL=4"

#Add trace.._
#asynSetTraceMask("PortA", -1, 0x9)        
#asynSetTraceIOMask("PortA", -1, 0x2)



iocInit
startPVAServer
